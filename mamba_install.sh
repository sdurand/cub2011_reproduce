#!/bin/bash
mamba install pytorch==1.10.1 torchvision==0.11.2 cudatoolkit=11.3 -c pytorch
mamba install timm tqdm matplotlib tensorboard==2.6.0 pandas -c torchinfo conda-forge