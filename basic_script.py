import argparse
import datetime
import sys
import time
from pathlib import Path

import timm
import torch
import numpy as np
from torchvision import transforms
from torch import nn
import matplotlib.pyplot as plt
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from data import Cub2011

options = {
    "base_lr": 0.001,
    "img_size": 448,
    "momentum": 0.9,
    "weight_decay": 1e-4,
    "step": 30,
    "gamma": 0.1,
    "data": "cub/",
    "epochs": 40,
    "batch_size": 16,
    'device': torch.device("cuda") if torch.cuda.is_available() else 'cpu',
    "num_workers": 4,
    "model_choice": "resnet50",
    "load_model": None,
    "model_save": "model_save",
    "tensorboard": "runs",
}


def parse_args():
    parser = argparse.ArgumentParser(
        description='Options for base model finetuning on CUB_200_2011 datasets'
    )
    parser.add_argument('--batch_size', type=int, default=16,
                        help='batch size for training')
    parser.add_argument('--base_lr', type=float, default=0.001,
                        help='base learning rate for training')
    parser.add_argument('--model_choice', type=str, required=True, default="resnet50",
                        help='model_choice: a string compatible with timm.create_model(name=model_choice)')
    parser.add_argument('--epochs', type=int, default=45,
                        help='epoch')
    parser.add_argument('--momentum', type=float, default=0.9,
                        help='momentum for SGD')
    parser.add_argument('--weight_decay', type=float, default=1e-4,
                        help='weight_decay for SGD')
    parser.add_argument('--num_workers', type=int, default=4,
                        help='Number of workers for the dataloaders')
    parser.add_argument('--img_size', type=int, default=448,
                        help='image size for transforms')
    parser.add_argument('--load_model', type=str, default=None,
                        help='Path to a model for picking up training')
    parser.add_argument('--gamma', type=float, default=0.1,
                        help='Gamma used for lr scheduler')
    parser.add_argument('--step', type=int, default=30,
                        help='step used for lr scheduler')
    parser.add_argument('--tensorboard', type=str, default="runs",
                        help='Path where to save tensorboard logs')
    parser.add_argument('--model_save', type=str, default="runs",
                        help='Path where to save models. Model will be saved as '
                             'model_save/model_choice_epoch.pth')
    parser.add_argument('--data', type=str, default="cub",
                        help='Path to the dataset. If the data has already been exctracted '
                             'the path should lead to a directory containing CUB_200_2011 folder with subfolders'
                             ' for attributes, images, parts, and '
                             'files for bounding boxes, images.txt, classes.txt etc.)')
    parser.add_argument("--top_k", type=int, default=5, help="Number of models to save. Top k models will be saved "
                                                             "when checkpointing")

    args = parser.parse_args()

    options = {
        "base_lr": args.base_lr,
        "img_size": args.img_size,
        "momentum": args.momentum,
        "weight_decay": args.weight_decay,
        "step": args.step,
        "gamma": args.gamma,
        "data": args.data,
        "epochs": args.epochs,
        "batch_size": args.batch_size,
        'device': torch.device("cuda") if torch.cuda.is_available() else 'cpu',
        "num_workers": args.num_workers,
        "model_choice": args.model_choice,
        "load_model": args.load_model,
        "model_save": args.model_save,
        "tensorboard": args.tensorboard,
        "top_k":args.top_k
    }
    return options


def get_test_acc(net, test_loader, device):
    net.eval()
    num_total = 0
    num_acc = 0
    with torch.no_grad():
        for imgs, labels in tqdm(test_loader, ncols=80):
            imgs = imgs.to(device)
            labels = labels.to(device)
            output = net(imgs)
            _, pred = torch.max(output, 1)
            num_acc += torch.sum(pred == labels.detach_())
            num_total += labels.size(0)
    return num_acc.detach().cpu().numpy() * 100 / num_total


if __name__ == "__main__":
    PROJ_ROOT = Path(__file__).parent
    if len(sys.argv) > 1:
        options = parse_args()
    train_transform_list = [
        transforms.RandomResizedCrop(options['img_size']),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]
    test_transforms_list = [
        transforms.Resize(int(options['img_size'] / 0.875)),
        transforms.CenterCrop(options['img_size']),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]
    # train_data = cub200(options['data'], train=True, transform=transforms.Compose(train_transform_list))
    # test_data = cub200(options['data'], train=False, transform=transforms.Compose(test_transforms_list))
    train_data = Cub2011(root=str(PROJ_ROOT / options["data"]), train=True,
                         transform=transforms.Compose(train_transform_list))
    test_data = Cub2011(root=str(PROJ_ROOT / options["data"]), train=False,
                        transform=transforms.Compose(test_transforms_list))

    train_loader = torch.utils.data.DataLoader(
        train_data, batch_size=options['batch_size'], shuffle=True, num_workers=options["num_workers"], pin_memory=True
    )
    test_loader = torch.utils.data.DataLoader(
        test_data, batch_size=4, shuffle=False, num_workers=options["num_workers"], pin_memory=True
    )

    #######################################################
    # Model and optims
    #######################################################
    device = options["device"]

    net = timm.create_model(model_name=options["model_choice"],
                            pretrained=True,
                            num_classes=200)

    net = net.to(device)
    if options["load_model"] is not None:
        net = torch.load(options["load_model"])
    if not Path(options["model_save"]).is_dir():
        Path(options["model_save"]).mkdir()
    criterion = nn.CrossEntropyLoss()
    solver = torch.optim.SGD(
        net.parameters(), lr=options['base_lr'], momentum=options['momentum'],
        weight_decay=options['weight_decay']
    )
    schedule = torch.optim.lr_scheduler.StepLR(solver, step_size=30, gamma=0.1)

    #######################################################
    # Training
    #######################################################
    epochs = np.arange(1, options['epochs'] + 1)
    test_acc = list()
    train_acc = list()
    print('Training process starts:...')
    if torch.cuda.device_count() > 1:
        print('More than one GPU are used...')
    print('Epoch\tTrainLoss\tTrainAcc\tTestAcc')
    print('-' * 50)
    best_acc = 0.0
    best_epoch = 0
    writer = SummaryWriter(f"runs/{options['model_choice']}_{options['img_size']}_" +
                           datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    hparam_dict = {
        "lr": options["base_lr"],
        "batch_size": options["batch_size"],
        "img_size": options["img_size"],
    }
    start = time.perf_counter()

    saved_accs = list()
    saved_models = list()
    top_k = options["top_k"]
    top_k = max(1, top_k)
    for epoch in range(options['epochs']):
        net.train(True)
        net.to(device=device)
        num_correct = 0
        train_loss_epoch = list()
        num_total = 0
        for imgs, labels in tqdm(train_loader, ncols=80):
            solver.zero_grad()
            imgs = imgs.to(device)
            labels = labels.to(device)
            output = net(imgs)
            loss = criterion(output, labels)
            _, pred = torch.max(output, 1)
            num_correct += torch.sum(pred == labels.detach_())
            num_total += labels.size(0)
            train_loss_epoch.append(loss.item())
            loss.backward()
            # nn.utils.clip_grad_norm_(net.parameters(), 1.0)
            solver.step()

        train_acc_epoch = num_correct.detach().cpu().numpy() * 100 / num_total
        avg_train_loss_epoch = sum(train_loss_epoch) / len(train_loss_epoch)
        test_acc_epoch = get_test_acc(net, test_loader, device)
        writer.add_scalar("train_acc", train_acc_epoch, epoch)
        writer.add_scalar("train_loss", avg_train_loss_epoch, epoch)
        writer.add_scalar("test_acc", train_acc_epoch, epoch)
        for name, weight in net.named_parameters():
            writer.add_histogram(name, weight, epoch)
            writer.add_histogram(f'{name}.grad', weight.grad, epoch)
        test_acc.append(test_acc_epoch)
        train_acc.append(train_acc_epoch)
        schedule.step()
        #  saving model if it's amongst the top 5
        if len(saved_accs) == 0:
            best_acc = test_acc_epoch
            best_epoch = epoch + 1
            savepath = Path(options["model_save"]) / f"{options['model_choice']}_epoch_{epoch + 1}_" \
                                                     f"test_acc_{test_acc_epoch:.2f}.pth"
            torch.save(net.cpu().state_dict(), savepath)
            saved_accs.append(test_acc_epoch)
            saved_models.append(savepath)
        else:
            is_best = False
            if len(saved_accs) < top_k:
                insert_idx = 0
                is_best = True
                for i, saved_acc in enumerate(saved_accs):
                    if test_acc_epoch > saved_acc:
                        insert_idx = i + 1
            else:
                if test_acc_epoch < saved_accs[0]:
                    continue
                for i, saved_acc in enumerate(saved_accs):
                    if test_acc_epoch > saved_acc:
                        is_best = True
                        insert_idx = i + 1
            if is_best:
                print(f"Found a new model among {top_k} best")
                savepath = Path(options["model_save"]) / f"{options['model_choice']}_epoch_{epoch + 1}_" \
                                                         f"test_acc_{test_acc_epoch:.2f}.pth"
                torch.save(net.cpu().state_dict(), savepath)
                saved_accs.insert(insert_idx, test_acc_epoch)
                saved_models.insert(insert_idx, savepath)
                if len(saved_accs) > top_k:
                    saved_accs.pop(0)
                    to_delete = saved_models.pop(0)
                    to_delete.unlink()

        if test_acc_epoch > best_acc:
            best_acc = test_acc_epoch
            best_epoch = epoch + 1
            print('*', end='')
        print(
            '{}\t{:.4f}\t{:.2f}%\t{:.2f}%'.format(epoch + 1, avg_train_loss_epoch, train_acc_epoch, test_acc_epoch))

    metric__dict = {
        "last_train_acc": train_acc[-1],
        "test_acc": best_acc,
        "best_epoch": best_epoch,
    }
    writer.add_hparams(hparam_dict, metric__dict)
    writer.close()
    elapsed = time.perf_counter() - start
    print("Total training time = {:.2f} m".format(elapsed / 60))
    plt.figure()
    plt.plot(epochs, test_acc, color='r', label='Test Acc')
    plt.plot(epochs, train_acc, color='b', label='Train Acc')

    plt.xlabel('epochs')
    plt.ylabel('Acc')
    plt.legend()
    plt.title(str(options['model_choice']))
    plt.savefig(str(options['model_choice']) + '.png')
    print("best_models=" + "\n".join([str(m) for m in saved_models]))
    print("best accs =" + "\n".join([str(acc) for acc in saved_accs]))
