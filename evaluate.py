import argparse
from pathlib import Path

import timm
import torch
from torchvision.transforms import transforms
from torchinfo import summary

from basic_lightning_script import CubModel
from basic_script import get_test_acc
from data import Cub2011


def load_model(path, model_name):
    path = Path(path)
    model: torch.nn.Module = timm.create_model(model_name, pretrained=False, num_classes=200)
    if path.suffix == ".ckpt":
        checkpoint = torch.load(path)
        pl_model = CubModel(
            loaders=None,
            model=model_name
        )
        pl_model.load_state_dict(checkpoint["state_dict"])
        model = pl_model.model
    else:
        state = torch.load(str(path))
        try:
            model.load_state_dict(state)
        except RuntimeError: # probably a model saved after encapsulation in nn.DataParallel
            model = torch.nn.DataParallel(module=model,device_ids=["cuda"])
            model.load_state_dict(state)
            model = model.module
        except AttributeError: # probably a model saved directly torch.load(model), and not a state_dict
            model = state
    return model


def parse_args():
    parser = argparse.ArgumentParser(
        description='Paths to model to convert'
    )
    parser.add_argument('--model_path', type=str, required=True,
                        help='Path to the checkpoint or model')

    parser.add_argument("--model_name", type=str, help="Timm name of the model")
    parser.add_argument("--save_path", type=str, help="Path to save the model."
                                                      "Use in case of loading from checkpoint and you want to "
                                                      "save the model state_dict, not the lightning module",
                        default=None)
    parser.add_argument('--data', type=str, default="cub",
                        help='Path to the dataset. If the data has already been exctracted '
                             'the path should lead to a directory containing CUB_200_2011 folder with subfolders'
                             ' for attributes, images, parts, and '
                             'files for bounding boxes, images.txt, classes.txt etc.)')
    parser.add_argument('--img_size', type=int, default=448,
                        help='image size for transforms')

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    PROJ_ROOT = Path(__file__).parent
    args = parse_args()
    path = args.model_path
    name = args.model_name
    datapath = args.data
    savepath = args.save_path
    img_size = args.img_size
    device = torch.device("cuda") if torch.cuda.is_available() else 'cpu'

    model = load_model(path=path,
                       model_name=name)

    stats = summary(model, (1, 3, img_size, img_size), device=device, verbose=0)
    print("=" * 60)
    print(f"Total params = {stats.total_params:,}")
    print(f"Trainable params = {stats.trainable_params:,}")
    print(f"Model size (MB) = {stats.float_to_megabytes(stats.total_params):.2f}")
    print("=" * 60)

    test_transforms_list = [
        transforms.Resize(int(img_size / 0.875)),
        transforms.CenterCrop(img_size),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]

    train_data = Cub2011(root=str(PROJ_ROOT / datapath), train=True,
                         transform=transforms.Compose(test_transforms_list))
    test_data = Cub2011(root=str(PROJ_ROOT / datapath), train=False,
                        transform=transforms.Compose(test_transforms_list))

    train_loader = torch.utils.data.DataLoader(
        train_data, batch_size=12, shuffle=False, num_workers=2, pin_memory=True
    )
    test_loader = torch.utils.data.DataLoader(
        test_data, batch_size=12, shuffle=False, num_workers=2, pin_memory=True
    )

    model = model.to(device)
    train_acc = get_test_acc(net=model, test_loader=train_loader, device=device)
    print("Accuracy on training set = {:.2f} %".format(train_acc))
    test_acc = get_test_acc(net=model, test_loader=test_loader, device=device)
    print("Accuracy on training set = {:.2f} %".format(test_acc))
    if savepath is not None:
        if "test_acc" not in str(path):
            # some models already have the test_acc in the name
            model_name = f"{Path(path).stem}_test_acc_{test_acc:.2f}.pth"
        else:
            model_name = f"{Path(path).stem}.pth"
        savepath = Path(savepath) / model_name

        torch.save(model.cpu().state_dict(), str(savepath))
