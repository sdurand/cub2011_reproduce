#!/bin/bash
conda install pytorch==1.10.1 torchvision==0.11.2 cudatoolkit=11.3 -c pytorch
conda install timm tqdm matplotlib tensorboard==2.6.0 pandas torchinfo -c conda-forge