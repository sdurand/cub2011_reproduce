import argparse
import copy
import datetime
import sys
from pathlib import Path

import pytorch_lightning as pl
import timm
import torch
from pytorch_lightning import Callback
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger
from torchvision import transforms

from data import Cub2011

options = {
    "base_lr": 0.001,
    "img_size": 446,
    "momentum": 0.9,
    "weight_decay": 1e-4,
    "step": 30,
    "gamma": 0.1,
    "data": "cub/",
    "epochs": 90,
    "batch_size": 16,
    'device': torch.device("cuda") if torch.cuda.is_available() else 'cpu',
    "num_workers": 4,
    "model_choice": "resnet50",
    "load_model": None,
    "model_save": "model_save",
    "tensorboard": "runs",
    "earlystopping": False,
    "es_criterion": "test_acc",
    "es_mode": "max",  # should be max for acc criterion, min for loss
    "gpus": 1,
    "resume_checkpoint": None,
    "accumulate_grad": 2,
    "top_k": 5
}


def parse_args():
    def str2bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(
        description='Options for base model finetuning on CUB_200_2011 datasets'
    )
    parser.add_argument('--batch_size', type=int, default=16,
                        help='batch size for training')
    parser.add_argument('--base_lr', type=float, default=0.001,
                        help='base learning rate for training')
    parser.add_argument('--model_choice', type=str, required=True,
                        help='model_choice: a string compatible with timm.create_model(name=model_choice)')
    parser.add_argument('--epochs', type=int, default=45,
                        help='epoch')
    parser.add_argument('--momentum', type=float, default=0.9,
                        help='momentum for SGD')
    parser.add_argument('--weight_decay', type=float, default=1e-4,
                        help='weight_decay for SGD')
    parser.add_argument('--num_workers', type=int, default=4,
                        help='Number of workers for the dataloaders')
    parser.add_argument('--img_size', type=int, default=448,
                        help='image size for transforms')
    parser.add_argument('--resume_checkpoint', type=str, default=None,
                        help='Path to a checkpoint for picking up training. This should be a .cpkt saved by'
                             ' the trainer, not just a model.pth')
    parser.add_argument('--gamma', type=float, default=0.1,
                        help='Gamma used for lr scheduler')
    parser.add_argument('--step', type=int, default=30,
                        help='step used for lr scheduler')
    parser.add_argument('--tensorboard', type=str, default="runs",
                        help='Path where to save tensorboard logs')
    parser.add_argument('--model_save', type=str, default="runs",
                        help='Path where to save models. Model will be saved as '
                             'model_save/model_choice_epoch.pth')
    parser.add_argument('--data', type=str, default="cub",
                        help='Path to the dataset. If the data has already been exctracted '
                             'the path should lead to a directory containing CUB_200_2011 folder with subfolders'
                             ' for attributes, images, parts, and '
                             'files for bounding boxes, images.txt, classes.txt etc.)')
    parser.add_argument("--earlystopping", type=str2bool, default=False, help="Stopping training early if set to True"
                                                                          "and training stagnates according to "
                                                                          "specified criterion",
                        const=True, nargs="?")
    parser.add_argument("--es_criterion", type=str, default="test_acc",
                        help="Early stopping metric: suggested 'test_acc'"
                             "or 'test_loss'. It will be used for "
                             "checkpointing as well")
    parser.add_argument("--es_mode", type=str, default="max", help="Mode for early stopping: max or min. If monitoring"
                                                                   " accuracy max should be chosen, if loss min should "
                                                                   "should be chosen. "
                                                                   "It will be used for checkpointing as well")
    parser.add_argument("--patience", type=int, default=4, help="Number of epochs to consider for early stopping")
    parser.add_argument("--min_delta", type=float, default=1e-5, help="Minimum value to consider as change for "
                                                                      "early stopping")
    parser.add_argument("--acc_grad", type=int, default=1,
                        help="Number of batch to accumulate gradients. if batch_size "
                             "is n, and acc_grad k the simulated batch size will be"
                             "n*k. "
                             "Use it if you need to have small batch size because"
                             "of computing limits.")
    parser.add_argument("--gpus", type=int, default=1, help="Number of gpus. Multigpu is handled by pytorch_lightning")
    parser.add_argument("--top_k", type=int, default=5, help="Number of models to save. Top k models will be saved "
                                                             "when checkpointing")

    args = parser.parse_args()

    options = {
        "base_lr": args.base_lr,
        "img_size": args.img_size,
        "momentum": args.momentum,
        "weight_decay": args.weight_decay,
        "step": args.step,
        "gamma": args.gamma,
        "data": args.data,
        "epochs": args.epochs,
        "batch_size": args.batch_size,
        'device': torch.device("cuda") if torch.cuda.is_available() else 'cpu',
        "num_workers": args.num_workers,
        "model_choice": args.model_choice,
        "model_save": args.model_save,
        "tensorboard": args.tensorboard,
        "earlystopping": args.earlystopping,
        "es_criterion": args.es_criterion,
        "es_mode": args.es_mode,  # should be max for acc criterion, min for loss
        "gpus": args.gpus,
        "resume_checkpoint": args.resume_checkpoint,
        "accumulate_grad": args.acc_grad,
        "top_k":args.top_k,
    }
    return options


class CubModel(pl.LightningModule):
    def __init__(self,
                 loaders,
                 lr: float = 0.001,
                 optim="SGD",
                 momentum: float = 0.9,
                 weight_decay: float = 1e-4,
                 model="resnet50",
                 pretrained: bool = True,
                 step: int = 30,
                 gamma: float = 0.1,
                 loss_fn=torch.nn.CrossEntropyLoss()):
        super().__init__()
        self.lr = lr
        self.optim = optim
        self.momentum = momentum
        self.weight_decay = weight_decay
        self.model = timm.create_model(model, pretrained=pretrained, num_classes=200)
        self.step = step
        self.gamma = gamma
        self.loaders = loaders
        self.loss_fn = loss_fn

    def configure_optimizers(self):
        if self.optim == "SGD":
            solver = torch.optim.SGD(self.model.parameters(),
                                     lr=self.lr, momentum=self.momentum, weight_decay=self.weight_decay)
        elif self.optim == "Adam":
            solver = torch.optim.Adam(self.model.parameters(),
                                      lr=self.lr, weight_decay=self.weight_decay)
        elif self.optim == "AdamW":
            solver = torch.optim.AdamW(self.model.parameters(),
                                       lr=self.lr, weight_decay=self.weight_decay)
        else:
            raise NotImplementedError(f"No support for optim {self.optim}")
        if self.step is not None:
            scheduler = torch.optim.lr_scheduler.StepLR(solver, step_size=self.step, gamma=self.gamma)
            return {"lr_scheduler": scheduler, "optimizer": solver}
        else:
            return solver

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_nb):
        imgs, labels = batch
        logits = self.model(imgs)
        loss = self.loss_fn(logits, labels)
        self.log("train_loss", loss, prog_bar=True, on_step=False, on_epoch=True)
        acc = (logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.log("train_acc", 100 * acc, prog_bar=True, on_step=False, on_epoch=True)

        return {"loss": loss, "acc": acc}

    def validation_step(self, batch, batch_idx):
        imgs, labels = batch

        logits = self.model(imgs)
        loss = self.loss_fn(logits, labels)
        acc = (logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.log("test_loss", loss, prog_bar=True, on_epoch=True)
        self.log('test_acc', 100 * acc, prog_bar=True, on_epoch=True)

        return loss, acc

    def test_step(self, batch, batch_idx):
        imgs, labels = batch
        logits = self.model(imgs)
        loss = self.loss_fn(logits, labels)
        acc = (logits.argmax(dim=1)).eq(labels).sum().item() / len(imgs)
        self.log("test_loss", loss, prog_bar=True)
        self.log('test_acc', 100 * acc, prog_bar=True)
        return loss, acc

    def train_dataloader(self):
        return self.loaders[0]

    def val_dataloader(self):

        return self.loaders[1]

    def test_dataloader(self):
        return self.loaders[2]


class SaveModel(Callback):
    def __init__(self, name, folder):
        super().__init__()
        self.name = name
        self.folder = folder

    def on_epoch_end(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        torch.save(copy.deepcopy(pl_module.model).cpu(), "{}/{}_{}.pth".format(self.folder,
                                                                               self.name, pl_module.current_epoch))


if __name__ == "__main__":
    PROJ_ROOT = Path(__file__).parent
    if len(sys.argv) > 1:
        options = parse_args()
    logger = TensorBoardLogger(save_dir=options["tensorboard"],
                               name=f"{options['model_choice']}_{options['img_size']}",
                               version=datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

    if not Path(options["model_save"]).is_dir():
        Path(options["model_save"]).mkdir()

    checkpoint_callback = ModelCheckpoint(
        monitor=options["es_criterion"],
        dirpath=options["model_save"],
        filename=f"{options['model_choice']}_{options['img_size']}-{{epoch:02d}}-{{{options['es_criterion']}:.4f}}",
        save_top_k=options["top_k"],
        mode=options["es_mode"],
    )
    callbacks = [pl.callbacks.progress.TQDMProgressBar(refresh_rate=1), checkpoint_callback]

    if options["earlystopping"]:
        callbacks += [EarlyStopping(monitor=options["es_criterion"],
                                    patience=4, min_delta=1e-5,
                                    mode=options["es_mode"])]

    trainer = pl.Trainer(gpus=options["gpus"],
                         max_epochs=options["epochs"],
                         val_check_interval=1.0,
                         default_root_dir=options["tensorboard"] + f"{options['model_choice']}_{options['img_size']}",
                         logger=[logger],
                         callbacks=callbacks,
                         accumulate_grad_batches=options["accumulate_grad"],
                         )
    train_transform_list = [
        transforms.RandomResizedCrop(options['img_size']),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]
    test_transforms_list = [
        transforms.Resize(int(options['img_size'] / 0.875)),
        transforms.CenterCrop(options['img_size']),
        transforms.ToTensor(),
        transforms.Normalize(mean=(0.485, 0.456, 0.406),
                             std=(0.229, 0.224, 0.225))
    ]
    train_data = Cub2011(root=str(PROJ_ROOT / options["data"]), train=True,
                         transform=transforms.Compose(train_transform_list))
    test_data = Cub2011(root=str(PROJ_ROOT / options["data"]), train=False,
                        transform=transforms.Compose(test_transforms_list))

    train_loader = torch.utils.data.DataLoader(
        train_data, batch_size=options['batch_size'], shuffle=True, num_workers=options["num_workers"], pin_memory=True
    )
    test_loader = torch.utils.data.DataLoader(
        test_data, batch_size=4, shuffle=False, num_workers=options["num_workers"], pin_memory=True
    )

    model = CubModel(
        loaders=(train_loader, test_loader, test_loader),
        momentum=options["momentum"],
        lr=options["base_lr"],
        step=options["step"],
        model=options["model_choice"]
    )
    ckpt_path = PROJ_ROOT / options["resume_checkpoint"] if options["resume_checkpoint"] is not None else None
    trainer.fit(model, ckpt_path=ckpt_path)
    res = trainer.test(model)
    print(res)
