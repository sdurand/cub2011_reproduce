# Reproducing baseline for CUB2011 classification

This is an extension of https://github.com/zhangyongshun/resnet_finetune_cub.
Main differences: relies on timm models, pytorch-lightning wrapping.

## Install

Tested on Ubuntu 20.04, with python 3.9.7.

Install requirements from requirements.txt using your favourite package manager.

If you use conda with [mamba](https://github.com/mamba-org/mamba) you can create a specific environment and use the 
install script.
```bash
mamba create -n cub2011 python=3.9
conda activate cub2011
sh mamba_install.sh
```

There is a conda script too, just replace mamba by conda in the above.

We use [timm](https://github.com/rwightman/pytorch-image-models) to create and download sota models.


## Dataset:

From http://www.vision.caltech.edu/visipedia/CUB-200-2011.html

Caltech-UCSD Birds-200-2011 (CUB-200-2011) is an extended version of the CUB-200 dataset, with roughly double the number of images per class and new part location annotations. For detailed information about the dataset, please see the technical report linked below.

*Number of categories: 200

*Number of images: 11,788

*Annotations per image: 15 Part Locations, 312 Binary Attributes, 1 Bounding Box

Only the class labels are used here.

The pytorch dataset version comes from https://github.com/TDeVries/cub2011_dataset.


## Run
Main script is basic_script.py
Go in and change the options dict for your preferred parameters then simply run:
```bash
python basic_script.py
```

Alternatively use command line arguments:
```bash
python basic_script.py --model_choice resnet50 --img_size 448 --batch_size 16 --epochs 90
```
Available options:
```bash
  -h, --help            show this help message and exit
  --batch_size BATCH_SIZE
                        batch size for training
  --base_lr BASE_LR     base learning rate for training
  --model_choice MODEL_CHOICE
                        model_choice: a string compatible with timm.create_model(name=model_choice)
  --epochs EPOCHS       epoch
  --momentum MOMENTUM   momentum for SGD
  --weight_decay WEIGHT_DECAY
                        weight_decay for SGD
  --num_workers NUM_WORKERS
                        Number of workers for the dataloaders
  --img_size IMG_SIZE   image size for transforms
  --load_model LOAD_MODEL
                        Path to a model for picking up training
  --gamma GAMMA         Gamma used for lr scheduler
  --step STEP           step used for lr scheduler
  --tensorboard TENSORBOARD
                        Path where to save tensorboard logs
  --model_save MODEL_SAVE
                        Path where to save models. Model will be saved as
                        model_save/model_choice_{img_size}-epoch={epoch}-test_acc={acc}.pth
  --data DATA           Path to the dataset. If the data has already been exctracted 
                        the path should lead to a directory containing CUB_200_2011 
                        folder with subfolders for attributes, images, parts, and files 
                        for bounding boxes, images.txt, classes.txt etc.)
  --top_k TOP_K         Number of models to save. Top k models will be saved when checkpointing

```
## Run with pytorch lightning
a similar script with pytorch_lightning can be run, with additional options:
* [EarlyStopping](https://pytorch-lightning.readthedocs.io/en/stable/extensions/generated/pytorch_lightning.callbacks.EarlyStopping.html)
* [Checkpointing](https://pytorch-lightning.readthedocs.io/en/stable/common/weights_loading.html)
* Multigpu (not tested)
* [Gradient accumulation](https://medium.com/huggingface/training-larger-batches-practical-tips-on-1-gpu-multi-gpu-distributed-setups-ec88c3e51255)

Options available on command line:
```bash
  -h, --help            show this help message and exit
  --batch_size BATCH_SIZE
                        batch size for training
  --base_lr BASE_LR     base learning rate for training
  --model_choice MODEL_CHOICE
                        model_choice: a string compatible with timm.create_model(name=model_choice)
  --epochs EPOCHS       epoch
  --momentum MOMENTUM   momentum for SGD
  --weight_decay WEIGHT_DECAY
                        weight_decay for SGD
  --num_workers NUM_WORKERS
                        Number of workers for the dataloaders
  --img_size IMG_SIZE   image size for transforms
  --resume_checkpoint RESUME_CHECKPOINT
                        Path to a checkpoint for picking up training. This should be a .cpkt 
                        saved by the trainer, not just a model.pth
  --gamma GAMMA         Gamma used for lr scheduler
  --step STEP           step used for lr scheduler
  --tensorboard TENSORBOARD
                        Path where to save tensorboard logs
  --model_save MODEL_SAVE
                        Path where to save models. Model will be saved as 
                        model_save/model_choice_{img_size}-epoch={epoch}-test_acc={acc}.ckpt
                        This is a checkpoint, it will save the optimizer, epoch etc.
  --data DATA           Path to the dataset. If the data has already been exctracted the path should 
                        lead to a directory containing CUB_200_2011 folder with subfolders for 
                        attributes, images, parts, and files for bounding boxes, images.txt, classes.txt etc.)
  --earlystopping [EARLYSTOPPING]
                        Stopping training early if set to Trueand training stagnates 
                        according to specified criterion
  --es_criterion ES_CRITERION
                        Early stopping metric: suggested 'test_acc'or 'test_loss'. 
                        It will be used for checkpointing as well
  --es_mode ES_MODE     Mode for early stopping: max or min. 
                        If monitoring accuracy max should be chosen, if loss min should should be chosen. 
                        It will be used for checkpointing as well
  --patience PATIENCE   Number of epochs to consider for early stopping
  --min_delta MIN_DELTA
                        Minimum value to consider as change for early stopping
  --acc_grad ACC_GRAD   Number of batch to accumulate gradients. if batch_size is n, 
                        and acc_grad k the simulated batch size will ben*k. 
                        Use it if you need to have small batch size becauseof computing limits.
  --gpus GPUS           Number of gpus. Multigpu is handled by pytorch_lightning
  --top_k TOP_K         Number of models to save. Top k models will be saved when checkpointing
```

## Getting 84~85 % acc with resnet50
The settings to reach this accuracy are:
* A pretrained resnet50 on Imagenet (the one from timm works fine, I assume the torchvision one should too)
* Train data augmentation: random horizontal flip, random resize crop (at 448 resize)
* Test data transform: Resize 510 then CenterCrop 448
* Scaling 0-1 + Imagenet normalization (check ```train_transform_list``` and ```test_transform_list``` for the transforms details)
* Optimizer: SGD with base learning rate 0.001, momentum 0.9, weight_decay 1e-4.
* Scheduler for learning_rate = divide by 10 every 30 epochs
* Batch_size: 16 (I assume bigger could be better but you need a good enough machine)
* Epochs: ~40 (it seems to reach > 80% after 30 epochs)

(Default setting should be those)
## Evaluate trained models

Use the evaluate.py file from command line. 
If you evaluate from a lightning checkpoint you can put a save_path argument to save the model only.


```bash
  -h, --help            show this help message and exit
  --model_path MODEL_PATH
                        Path to the checkpoint or model
  --model_name MODEL_NAME
                        Timm name of the model
  --save_path SAVE_PATH
                        Path to save the model. Use in case of loading from checkpoint 
                        and you want to save the model state_dict, not the lightning module
  --data DATA           Path to the dataset. If the data has already been exctracted 
                        the path should lead to a directory containing CUB_200_2011 folder 
                        with subfolders for attributes, images, parts, and files
                         for bounding boxes, images.txt, classes.txt etc.)
  --img_size IMG_SIZE   image size for transforms
```
## Results

Comparison with ecaresnet50d_pruned and efficientnet_b3_pruned:

|          model         | params / size (MB) | Accuracy on test set (%) | Best epoch | Total training time |
|:----------------------:|:------------------:|:------------------------:|:----------:|:-------------------:|
|        resnet50        | 23,917,832 / 95.67 |           85.38          |     39     |        1h49m        |
|   ecaresnet50d_pruned  | 18,321,313 / 73.29 |           82.03          |     15     |        0h37m        |
| efficientnet_b3_pruned |  8,625,420 / 34.50 |           85.16          |     33     |         1h6         |

Note: Training stopped probably too early for ecaresnet50d_pruned, I believe it could have converged higher (but still 
not as high as the resnet50 or efficientnet_b3_pruned)
```bash
python evaluate.py --model_path best_models/efficientnet_b3_pruned_446-epoch\=33-test_acc\=85.1571.pth  \
--model_name efficientnet_b3_pruned --img_size 446
python evaluate.py --model_path best_models/resnet50_446-epoch\=39-test_acc\=85.3814.pth \
--model_name resnet50 --img_size 446
python evaluate.py --model_path best_models/ecaresnet50d_pruned_448-epoch\=15-test_acc\=82.0504.pth \
--model_name ecaresnet50d_pruned
```


## Reusing models

State dict of the models reported above are in folder best_models.

To reuse them create a model with timm (no need for pretrained here) and use torch.load.
For resnets the torchvision models are compatible, no need for timm.
```python
import torchvision
import torch
import timm

# model = torchvision.models.resnet50(pretrained=False, num_classes=200)
model = timm.create_model("resnet50", pretrained=False, num_classes=200)
model.load_state_dict(torch.load("best_models/resnet50_446-epoch=39-test_acc=85.3814.pth"))
```