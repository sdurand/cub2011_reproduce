import torch.nn
import torch.nn as nn
from torchvision import transforms
import torchvision.datasets.cifar
from torchvision.models.resnet import resnet50

def eval(dataset, model):
    cpt = 0
    with torch.no_grad():
        model = model.eval().cpu()
        for img, label in dataset:
            # need batch dim
            img = img.unsqueeze(0)
            logits = model(img)
            pred = logits.argmax().item()
            if pred == label:
                cpt += 1

    print(f"Acc = {cpt} / {len(dataset)}")
    print(f"% acc = {100 * cpt/len(dataset):.3f} %")

if __name__ == "__main__":
    model = nn.Sequential(*[
        nn.Flatten(),
        nn.Linear(32 * 32 * 3, 100),
        nn.ReLU(),
        nn.Linear(100, 100),
        nn.ReLU(),
        nn.Linear(100, 100),
        nn.ReLU(),
        nn.Linear(100, 100),
        nn.ReLU(),
        nn.Linear(100, 10)
    ])
    cifar_transforms = transforms.Compose([transforms.ToTensor(),
                                           # transforms.Normalize(mean=(0.4914, 0.4822, 0.4465),
                                           #                      std=(0.2023, 0.1994, 0.2010))
                                           ])

    train_data = torchvision.datasets.CIFAR10(root="./datasets",
                                              download=True,
                                              transform=cifar_transforms,
                                              train=True
                                              )

    test_data = torchvision.datasets.CIFAR10(root="./datasets",
                                             download=True,
                                             transform=cifar_transforms,
                                             train=False
                                             )
    model.load_state_dict(torch.load("best_models/cifar10/simpleFNN_epoch_34_test_acc_55.62.pth"))
    eval(train_data, model)
    eval(test_data, model)

    model = resnet50(num_classes=10)
    model.load_state_dict(torch.load("best_models/cifar10/resnet50_cifar_10_epoch_44_test_acc_85.27.pth"))

    # this model was trained with normalization.

    cifar_transforms = transforms.Compose([transforms.ToTensor(),
                                           transforms.Normalize(mean=(0.4914, 0.4822, 0.4465),
                                                                std=(0.2023, 0.1994, 0.2010))
                                           ])

    train_data = torchvision.datasets.CIFAR10(root="./datasets",
                                              download=True,
                                              transform=cifar_transforms,
                                              train=True
                                              )

    test_data = torchvision.datasets.CIFAR10(root="./datasets",
                                             download=True,
                                             transform=cifar_transforms,
                                             train=False
                                             )
    eval(train_data, model)
    eval(test_data, model)


